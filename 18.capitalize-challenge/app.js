const myString = "hello";

const firstLetter = myString[0];

const newString1 = firstLetter.toUpperCase() + myString.substring(1);

const newString2 = firstLetter.toUpperCase() + myString.slice(1);

console.log(newString1, newString2)
